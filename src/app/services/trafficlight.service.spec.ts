import { TestBed } from '@angular/core/testing';

import { TrafficLightService } from './trafficlight.service';

describe('TrafficlightService', () => {
  let service: TrafficLightService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrafficLightService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
