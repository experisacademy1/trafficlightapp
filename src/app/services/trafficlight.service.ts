import { Injectable } from "@angular/core";
import { Observable, timer } from "rxjs";
import { HttpClient } from '@angular/common/http';



@Injectable({
    providedIn: 'root'
})
export class TrafficLightService {

    apiUrl = "https://localhost:7264"

    constructor(private http: HttpClient) { }

    startTrafficLight(): Observable<any> {
        return this.http.get<any[]>(`${this.apiUrl}/start`);
    }

    setLight(timer: number): Observable<any> {
        return this.http.put<any[]>(`${this.apiUrl}/setlight?timer=${timer}`, timer);
    }

    pushButton(timer: number): Observable<any> {
        return this.http.put<any[]>(`${this.apiUrl}/pushbutton?timer=${timer}`, timer);
    }
}