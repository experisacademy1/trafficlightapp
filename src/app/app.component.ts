import { Component } from '@angular/core';
import { TrafficLightService } from './services/trafficlight.service';
import { catchError, throwError, timer } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'traffic-light-app';

 trafficData: any;

  constructor(
    private readonly trafficLightService : TrafficLightService) { 
      this.trafficData = [];
    }

    timeLeft: number;
    subscribeTimer: any;
    interval : any;
    red: string;
    yellow: string;
    green: string;


    startTimer() {
      this.timeLeft = this.trafficData.redTimer;
      this.red = "red";
      this.interval = setInterval(() => { 
        if(this.timeLeft > 0) {
          this.timeLeft--;
        } else {
          if(this.trafficData.lightState === "Red") {           
            this.trafficLightService.setLight(this.trafficData.redTimer).subscribe((data:any)=> { 
            this.trafficData = data;
            this.updateLight();
          })}
          else if(this.trafficData.lightState === "Green") {
            this.trafficLightService.setLight(this.trafficData.greenTimer).subscribe((data:any)=> { 
              this.trafficData = data;
              this.updateLight(); 
          })}
          else { 
            this.trafficLightService.setLight(0).subscribe((data:any)=> { 
              this.trafficData = data;
              this.updateLight(); 
           })
        }}
      }, 1000)
    }

    ngOnInit() { 
      this.trafficLightService.startTrafficLight().subscribe((data:any) => {
        this.trafficData = data;
        this.startTimer();
      }); 
    }

    updateLight() {
      if(this.trafficData.lightState === "Red") { 
        this.timeLeft = this.trafficData.redTimer; 
        this.yellow = "#444601";
        this.red = "red";
      }

        if (this.trafficData.lightState === "YellowGreen") {
          this.timeLeft = 5;
          this.red = "red";
          this.yellow = "yellow";
        }

        if (this.trafficData.lightState === "Green") {
          this.timeLeft = this.trafficData.greenTimer;
          this.red = "#3b0000";
          this.yellow = "#444601";
          this.green = "green";
        }

        if (this.trafficData.lightState === "Yellow") {
          this.timeLeft = 5;
          this.red = "#3b0000";
          this.yellow = "yellow";
          this.green = "#003b0f";
        }
    }

    PushButton() {
      this.trafficLightService.pushButton(this.timeLeft)
        .pipe(
          catchError((error) => {
            window.alert(error.error)
            return error;
          })
        )
        .subscribe((data: any) => {
          this.trafficData = data;
          this.timeLeft = this.trafficData.greenTimer;
        });
    }
}
