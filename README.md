# Trafficlight App

This project simulates a traffic light. This is the front-end part of the application. You can find the backend part at https://gitlab.com/experisacademy1/trafficlightbackend

## Table of Contents

- [Clone](#clone)
- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Start](#start)
- [Contributors](#contributors)
- [Known bugs](#known-bugs)

## Clone

You can clone this repository for your own uses through the following command in your terminal.
```
$cd (Chosen folder)
$git clone https://gitlab.com/experisacademy1/trafficlightapp.git
```

## Install

This website runs on angular. If you don't have it you can install it using the following command

```
npm install -g @angular/cli
```

## Usage

The goal with this application is to simulate a traffic light. There is a button you can press while the light is green to extend the timer up to a specified limit (Set in backend). Otherwise an error message will pop up.


## Start

Run `ng serve` to start the code in your browser. It will be accessible from http://localhost:4200/


## Contributors

Erik - @ermo1222

## Known Bugs

I'm not a very good designer, so uhhh please excuse the fact that the pole isn't connecting to the traffic light itself.
